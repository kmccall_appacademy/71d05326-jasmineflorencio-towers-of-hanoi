class TowersOfHanoi
  attr_reader :towers
  def initialize
    @towers = [[3, 2, 1],[],[]]
  end

  def play
    # until won?
      render
      puts "Select 1, 2, or 3"
      pick_from = gets.chomp
      puts "Move to 1, 2, or 3"
      move_to = gets.chomp
    # end
  end

  def render
    puts @towers
  end


  def move(from_tower, to_tower)
    if from_tower == 0
      tower_pop = self.towers[0].pop
    elsif from_tower == 1
      tower_pop = self.towers[1].pop
    elsif from_tower == 2
      tower_pop = self.towers[2].pop
    else
      "Invalid choice. Please pick a disc from tower 1, 2, or 3."
    end

    if to_tower == 0
      self.towers[0].push(tower_pop)
    elsif to_tower == 1
      self.towers[1].push(tower_pop)
    elsif to_tower == 2
      self.towers[2].push(tower_pop)
    else
      "Invalid choice. Move disc to tower 1, 2, or 3."
    end
  end

  def valid_move?(from_tower, to_tower)
  return false if from_tower == to_tower
  if from_tower == 0
    return false if self.towers[0].empty?
  elsif from_tower == 1
    return false if self.towers[1].empty?
  elsif from_tower == 2
    return false if self.towers[2].empty?
  end

  from_disc = self.towers[from_tower].pop
  to_disc = self.towers[to_tower][-1]
  return false if from_disc.nil?
  unless to_disc == nil
    return from_disc < to_disc
  end

  true
  end

  def won?
    return true if self.towers[0].empty? && self.towers[1].empty?
    return true if self.towers[0].empty? && self.towers[2].empty?

  end

end
